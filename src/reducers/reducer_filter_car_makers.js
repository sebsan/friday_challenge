import { FILTER_CAR_MAKER } from "../actions/types";

export default function(state = [], action) {
  switch (action.type) {
    case FILTER_CAR_MAKER:
      return action.payload;
  }
  return state;
}
