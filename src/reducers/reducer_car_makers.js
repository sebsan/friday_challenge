// this reducer returns some predefined test data for now

export default function(state = [], action) {
  return [
    { name: "BMW", synonyms: [] },
    { name: "DaimlerBenz", synonyms: ["Mercedes", "Daimler", "Benz"] },
    { name: "Ferrari", synonyms: [] },
    { name: "Lamborghini", synonyms: [] },
    { name: "Jaguar", synonyms: ["Jag"] },
    { name: "Suzuki", synonyms: [] },
    { name: "Tesla", synonyms: [] },
    { name: "Trabant", synonyms: ["Trabbi", "Rennpappe"] },
    { name: "Toyota", synonyms: [] },
    { name: "Volkswagen", synonyms: ["VW"] }
  ];
}
