import { combineReducers } from "redux";
import { reducer as FormReducer } from "redux-form";
import CarMakersReducer from "./reducer_car_makers";
import ActiveCarMakerReducer from "./reducer_active_car_maker";
import FilterCarMakerReducer from "./reducer_filter_car_makers";

const rootReducer = combineReducers({
  carMakers: CarMakersReducer,
  filteredCarMakers: FilterCarMakerReducer,
  activeCarMaker: ActiveCarMakerReducer,
  form: FormReducer
});

export default rootReducer;
