import { SELECT_CAR_MAKER } from "../actions/types";

export default function(state = null, action) {
  switch (action.type) {
    case SELECT_CAR_MAKER:
      return action.payload;
  }
  return state;
}
