import React, { Component } from "react";
import CarMakerInput from "../containers/car_maker_input";
import CarMakerList from "../containers/car_maker_list";
import CarMakerDetail from "../containers/car_maker_detail";

export default class App extends Component {
  render() {
    return (
      <div>
        <CarMakerInput />
        <CarMakerList />
        <CarMakerDetail />
      </div>
    );
  }
}
