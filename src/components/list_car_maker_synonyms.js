import React, { Component } from "react";

const ListCarMakerSynonyms = ({ carMaker }) => {
  if (carMaker.synonyms && carMaker.synonyms > 0) {
    <span>
      (
      {carMaker.synonyms.map(carMakerSynonym => {
        return { carMakerSynonym };
      })}
      )
    </span>;
  }
};

export default ListCarMakerSynonyms;
