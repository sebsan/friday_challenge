import React, { Component } from "react";
import { connect } from "react-redux";
import { selectCarMaker } from "../actions/index";
import { bindActionCreators } from "redux";
import { ListCarMakerSynonyms } from "../components/list_car_maker_synonyms";

class CarMakerList extends Component {
  renderList(carMakerList) {
    return carMakerList.map(carMaker => {
      return (
        <li
          key={carMaker.name}
          onClick={() => this.props.selectCarMaker(carMaker)}
          className="list-group-item"
        >
          {carMaker.name}
        </li>
      );
    });
  }

  render() {
    // check if we have filtered car makers to make the list, otherwise use initial set of data
    const carMakersToList =
      this.props.filteredCarMakers === undefined ||
      this.props.filteredCarMakers.length == 0
        ? this.props.carMakers
        : this.props.filteredCarMakers;

    return (
      <ul className="list-group col-sm-4">
        {this.renderList(carMakersToList)}
      </ul>
    );
  }
}

function mapStateToProps(state) {
  return {
    carMakers: state.carMakers,
    filteredCarMakers: state.filteredCarMakers
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ selectCarMaker: selectCarMaker }, dispatch);
}

// make this component a container with state managed by redux
export default connect(mapStateToProps, mapDispatchToProps)(CarMakerList);
