import React, { Component } from "react";
import { connect } from "react-redux";

class CarMakerDetail extends Component {
  render() {
    if (!this.props.activeCarMaker) {
      return <div>Click to select a Car Maker</div>;
    }

    return (
      <div>
        <h3>Details for:</h3>
        <div>
          Name: {this.props.activeCarMaker.name}
        </div>
        <div>
          synonyms: {this.props.activeCarMaker.synonyms}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    activeCarMaker: state.activeCarMaker
  };
}

export default connect(mapStateToProps)(CarMakerDetail);
