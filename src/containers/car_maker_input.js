import React, { Component } from "react";
import { filterCarMaker } from "../actions/index";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class CarMakerInput extends Component {
  renderCarMakerInputField(field) {
    return <input type="text" {...field.input} />;
  }

  handleOnChange(event, newValue, previousValue) {
    this.props.filterCarMaker(this.props.carMakers, newValue);
  }

  render() {
    return (
      <div className="form-group">
        <label>Type to search for your car make</label>
        <br />
        <Field
          className="form-control"
          name="carMakerName"
          onChange={this.handleOnChange.bind(this)}
          component={this.renderCarMakerInputField}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    carMakers: state.carMakers
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ filterCarMaker: filterCarMaker }, dispatch);
}

export default reduxForm({
  form: "CarMakerInput"
})(connect(mapStateToProps, mapDispatchToProps)(CarMakerInput));
