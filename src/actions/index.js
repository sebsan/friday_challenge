import { SELECT_CAR_MAKER, FILTER_CAR_MAKER } from "./types";

export function selectCarMaker(activeCarMaker) {
  return {
    type: SELECT_CAR_MAKER,
    payload: activeCarMaker
  };
}

export function filterCarMaker(carMakers, filterString) {
  let filteredCarMakers = [];
  if (carMakers) {
    filteredCarMakers = carMakers.filter(carMaker => {
      return (
        carMaker.name.toLowerCase().indexOf(filterString.toLowerCase()) > -1 ||
        carMaker.synonyms.reduce(
          (accumulator, currentValue, currentIndex, array) => {
            return (
              currentValue.toLowerCase().indexOf(filterString.toLowerCase()) >
                -1 || accumulator
            );
          },
          false
        )
      );
    });
  }

  return {
    type: FILTER_CAR_MAKER,
    payload: filteredCarMakers
  };
}
