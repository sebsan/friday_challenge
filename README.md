# Friday Challenge

## How to Install
Make sure you have Node.JS installed.
Follow these instructions:

```
git clone git@gitlab.com:sebsan/friday_challenge.git
cd friday_challenge
npm install
npm run
```

It now starts the Webpack Dev Server, and the application can be accessed under `http://localhost:8080`

The tests can be run with

```
npm test
```

## Description of my solution

I implemented 3 React components - `CarMakerList`, `CarMakerDetails`, and `CarMakerInput`.
I have based the application on a simple React Redux Boilerplate with Mocha / Chai for testing.
All 3 components manage their state via Redux. To manage the state of the application,
we require 3 reducers plus a form reducer:
* car maker reducer - this initialises the application by just returning some pre-defined data for now. This can be replaced with an API call in the future
* active car maker reducer - manages the state of the selected car maker
* car maker filter - manages the state of filtered car makers

In order to change the state of the application, we have 2 possible actions:
* `selectCarMaker(carMaker)` - selects a car maker
* `filterCarMaker(carMakers, searchCriteria)`- creates a state for filtered car makers, a list filtered based on a given search criteria - a string


To give an idea how to apply TDD and BDD on all levels of the application design, I exemplary implemented the following tests:

* `actions/filter_car_makers_test` - tests if the filter action works
* `components/app_test` - tests if the application renders plus it actually executes a filter action and see if it works by entering a search term
* `components/car_maker_list_test` - tests whether the car maker list renders
* `reducers/car_makers_filter_test` - tests whether the car maker filter reducer works


I already added a synonym field to the test data. We can simply improve our filter action to filter based on the given synonyms additionally to filtering by car maker name.
