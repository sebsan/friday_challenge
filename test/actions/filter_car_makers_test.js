import { expect } from "../test_helper";
import { filterCarMaker } from "../../src/actions";
import { FILTER_CAR_MAKER } from "../../src/actions/types";

describe("Car Maker Filter Action", () => {
  it("filters the right car maker based on a given search term", () => {
    const newState = filterCarMaker(
      [
        { name: "BMW", synonyms: [] },
        { name: "DaimlerBenz", synonyms: ["Mercedes", "Daimler", "Benz"] },
        { name: "Ferrari", synonyms: [] }
      ],
      "BM"
    );
    expect(newState.type).to.be.equal(FILTER_CAR_MAKER);
    expect(newState.payload).to.be
      .instanceof(Array)
      .that.includes({ name: "BMW", synonyms: [] });
  });
});
