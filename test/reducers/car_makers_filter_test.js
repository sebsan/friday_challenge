import { expect } from "../test_helper";
import reducerFilterCarMakers from "../../src/reducers/reducer_filter_car_makers";
import { FILTER_CAR_MAKER } from "../../src/actions/types";

describe("Car Maker Filter Reducer", () => {
  it("doesnt do anything for an unknown action", () => {
    const action = {type: "unknown_action", payload: { name: 'BMW', synonyms: [] }};
    expect(reducerFilterCarMakers([], action)).to.be.instanceof(Array).that.is.empty;
  });

  it("always gives me the filtered result payload", () => {
    const filterAction = {type: FILTER_CAR_MAKER, payload: { name: 'BMW', synonyms: [] }};
    expect(reducerFilterCarMakers([], filterAction)).to.be.eql({ name: 'BMW', synonyms: [] });
  });
});
