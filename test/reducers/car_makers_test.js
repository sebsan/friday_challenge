import { expect } from "../test_helper";
import reducerCarMakers from "../../src/reducers/reducer_car_makers";

describe("Car Maker Reducer", () => {
  it("always gives me the predefined car maker list no matter which action", () => {
    const action = {type: 'unknown_action', payload: {}};
    expect(reducerCarMakers([], action)).to.be.instanceof(Array).that.includes({ name: 'BMW', synonyms: [] });
  });
});
