import { renderComponent , expect } from '../test_helper';
import CarMakerList from '../../src/containers/car_maker_list';


describe('Car Maker List' , () => {
  let component;

  beforeEach(() => {
    component = renderComponent(CarMakerList);
  });

  it('renders the component', () => {
    expect(component).to.exist;
  });

  it('it should contain the predefined test data', () => {
    expect(component.find('li').length).to.be.equal(10);
  });
});
