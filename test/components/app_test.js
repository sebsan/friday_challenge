import { renderComponent , expect } from '../test_helper';
import App from '../../src/components/app';

describe('App' , () => {
  let component;

  beforeEach(() => {
    component = renderComponent(App);
  });

  it('renders the app', () => {
    expect(component).to.exist;
  });

  describe('User enters search values', () => {
    beforeEach(() => {
      component.find('input').simulate('change', 'BMW');
    });

    it('it shows a filtered list with BMW as value', () => {
      expect(component.find('li').length).to.be.equal(1);
      expect(component.find('li')).to.have.text("BMW");
    })

  });
});
